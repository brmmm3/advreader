# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [2.4.3] - 2024-02-28

### Improved

- Check for valid encoder string, if provided.

## [2.4.2] - 2024-02-23

### Fixed

- Do not crash if encoding is set and encoding_errors is None.

## [2.4.1] - 2024-02-23

### Fixed

- Remove compiler option 'Z' because it is only supported in nightly builds.

## [2.4.0] - 2024-02-22

### Improved

- Add support for Python 3.13.
- Update dependencies.
- Improve encoding support.

### Changed

- API CHANGE: Following AdvReader creation arguments changed:
    encoder -> encoding
    allow_invalid_utf8 (bool) -> encoding_errors (str)

## [2.3.0] - 2024-07-10

### Improved

- Detect number with leading 00.. as Bytes.

### Fixed

- Fix python modul export

## [2.2.1] - 2024-07-09

### Added

- Add method 'push_back' to the iterator.

### Improved

- Update dependencies.

### Fixed

- Fix clippy warnings.

## [2.2.0] - 2024-01-26

### Added

Add platform independent ANSI encoder ansi2.

## [2.1.1] - 2024-01-23

### Fixed

Better error handling when encoding text fails.

## [2.1.0] - 2024-01-19

### Improved

Add support for system dependent encodings (ansi and oem). Especially for Windows needed.

## [2.0.0] - 2023-11-10

### Fixed

Rewrite several parts to fix bugs and add unit and integration tests.

## [1.3.0] - 2023-09-27

### Improved

Improve performance.

### Fixed

Fix a possible crash.

## [1.2.0] - 2023-09-10

### Improved

Fix Cargo.toml
Add build script and update dependencies.

## [1.1.0] - 2022-04-22

### Changed

Update dependencies.
Switch from setup.py to pyproject.toml.
Fix clippy findings.

## [1.0.0] - 2021-08-15

### Fixed

Fix block mode handling in multithreaded use cases.

## [0.9.0] - 2021-08-13

### Added

Add additional optional return value in block mode callback function.

### Fixed

Fixed block mode handling.

## [0.8.0] - 2021-08-13

### Fixed

Another fix in block mode handling.

## [0.7.0] - 2021-08-12

### Added

Add support for block mode.

## [0.6.0] - 2021-08-11

### Added

Fix support for big integer numbers.

## [0.5.0] - 2021-08-11

### Added

Support for big integer numbers. Will be automatically converted into float.
