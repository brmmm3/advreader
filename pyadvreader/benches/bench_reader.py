import sys
import time

import pyadvreader as reader

t1 = time.time()
with reader.AdvReader(
    "../testdata/big.txt" if len(sys.argv) < 2 else sys.argv[1],
    encode=True,
    allow_invalid_utf8=True,
    convert2numbers=True,
    bool_false=b"False",
    bool_true=b"True",
    return_type=True,
) as R:
    while (s := R.next()) is not None:
        pass
print(f"dt={time.time() - t1}")
