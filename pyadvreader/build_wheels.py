import os
import sys
import subprocess
import concurrent.futures
from typing import List


def Run(args: List[str]) -> subprocess.CompletedProcess:
    print("RUN:", " ".join(args))
    if os.name == 'nt':
        return subprocess.run(args, shell=True, capture_output=True)
    return subprocess.run(" ".join(args), shell=True, capture_output=True)


def ShowResult(title: str, prc: subprocess.CompletedProcess):
    stdout = prc.stdout.decode("utf-8")
    stderr = prc.stderr.decode("utf-8")
    if prc.returncode != 0:
        print(f"'{title}' failed with error code {prc.returncode}")
        print(stderr)
    elif not stdout:
        stdout = stderr
    return stdout, prc.returncode


def BuildWheel(versions_dir: str, version: str, python_exe: str, features: str, bDebug: bool) -> int:
    print(f"Building wheel for Python version {version}...")
    python_path = f"{versions_dir}/{version}/{python_exe}"
    cmd = ["maturin", "build", "--strip", "-i", python_path]
    if not bDebug:
        cmd.insert(2, "--release")
    if features:
        cmd.extend(["--features", f'{features.replace(",", " ")}'])
    maturin_build = Run(cmd)
    stdout, returncode = ShowResult("maturin build", maturin_build)
    if returncode != 0:
        return returncode
    builtWheel = [line for line in stdout.splitlines() if "Built wheel for CPython" in line]
    if not builtWheel:
        print("No wheel built!")
        print(stdout)
        return 1
    wheel_path = builtWheel[0].split(" to ")[1]

    upgrade_pip = Run([python_path, "-m", "pip", "install", "-U", "pip"])
    stdout, returncode = ShowResult("pip install -U pip", upgrade_pip)
    if returncode != 0:
        return returncode

    upgrade_pytest = Run([python_path, "-m", "pip", "install", "-U", "pytest"])
    stdout, returncode = ShowResult("pip install -U pytest", upgrade_pytest)
    if returncode != 0:
        return returncode

    install_wheel = Run(
        [python_path, "-m", "pip", "install", "--force-reinstall", wheel_path])
    stdout, returncode = ShowResult("install wheel", install_wheel)
    if returncode != 0:
        return returncode

    run_pytest = Run([python_path, "-m", "pytest"])
    stdout, returncode = ShowResult("pytest", run_pytest)
    print(stdout)
    if returncode != 0 and returncode != 5:
        return returncode
    return 0


def BuildWheels(versionsDir: str | None, versions: List[str] | None, features: str | None, bDebug: bool) -> bool:
    if not versionsDir:
        print("ERROR: No valid pyenv versions directory!")
        return False
    if not versions:
        print("ERROR: No versions to build Python wheel!")
        return False
    print(f"Building wheel for Python versions {bDebug=}:")
    print("\n".join(versions))
    pythonExe = "python.exe" if os.name == 'nt' else "bin/python"
    futures = {}
    with concurrent.futures.ThreadPoolExecutor(max_workers=4) as executor:
        for version in versions:
            futures[version] = executor.submit(BuildWheel, versionsDir, version, pythonExe, features, bDebug)
        for version, future in futures.items():
            returncode = future.result()
            if returncode != 0:
                print(f"ERROR: Building wheel for Python version {version} failed with error code {returncode}!")
    return True


def GetPyEnvVersions() -> List[str] | None:
    pyEnvVersions = Run(["pyenv", "versions"])
    stdout, returncode = ShowResult("pyenv versions", pyEnvVersions)
    if pyEnvVersions.returncode != 0:
        print(f"ERROR: Failed to get pyenv versions with error {pyEnvVersions.returncode}!")
        return None
    return [version.lstrip("*").strip().split()[0]
            for version in stdout.splitlines()
            if "system" not in version and " 2.7." not in version]


def GetPyEnvVersionsDir() -> str | None:
    pythonPath = Run(["pyenv", "which", "python"])
    if pythonPath.returncode != 0:
        print(f"ERROR: 'pyenv which python' failed with error code {pythonPath.returncode}")
        print(pythonPath.stderr.decode("utf-8"))
        return None
    return pythonPath.stdout.decode("utf-8").rsplit("versions", 1)[0] + "versions"


if __name__ == "__main__":
    if "--versions" in sys.argv:
        pyenvVersions = GetPyEnvVersions()
        versions = sys.argv[sys.argv.index("--versions") + 1].split(",")
        if versions == ["*"]:
            versions = pyenvVersions
        versionsSet = set()
        for v in versions:
            for pv in pyenvVersions:
                if pv.startswith(v):
                    versionsSet.add(pv)
                    break
        versions = sorted(versionsSet)
    else:
        versions = [sys.version.split()[0]]
    features = None
    if "--features" in sys.argv:
        features = sys.argv[sys.argv.index("--features") + 1]
    if not BuildWheels(GetPyEnvVersionsDir(), versions, features, "--debug" in sys.argv):
        sys.exit(1)
