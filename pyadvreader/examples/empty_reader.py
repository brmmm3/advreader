import os

import pyadvreader as reader


dirName = "testdata" if os.path.exists("testdata") else "../testdata"

with reader.AdvReader(f"{dirName}/empty.txt") as R:
    while s := R.next():
        print(f"#{s}#")
