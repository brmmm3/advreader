import os

import pyadvreader as reader


dirName = "testdata" if os.path.exists("testdata") else "../testdata"

print("Read whole file...")
with reader.AdvReader(
    f"{dirName}/example.txt",
    encode_comments=True,
    encoding="windows-1252",
    encoding_errors="replace",
    convert2numbers=True,
    bool_false=b"False",
    bool_true=b"True",
    return_type=True,
) as R:
    while (s := R.next()) is not None:
        print(s)

print("\nRead just 10 items...")
with reader.AdvReader(f"{dirName}/example.txt") as R:
    for _ in range(10):
        s = R.next()
        print(f"{type(s)}#{s}#")

print(dir(reader))
print(dir(reader.ReturnValueEnum))
print(reader.ReturnValueEnum.BYTES)
print(reader.ReturnValueEnum.STRING)
print(reader.ReturnValueEnum.toStr(4))
print(reader.ReturnValueEnum.toStr(8))
print(reader.ReturnValueEnum.toStr(12))
print(reader.ReturnValueEnum.toStr(14))
