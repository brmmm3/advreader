use std::io::Error;
use std::path::PathBuf;
use std::str;

use advreader::AdvReturnValue;
use pyo3::exceptions::{PyFileNotFoundError, PyValueError};
use pyo3::prelude::*;
use pyo3::types::{PyTuple, PyType};

pub mod return_value;

use return_value::{AdvReturnValueEnum, ReturnValueEnum};

#[pyclass(module = "pyadvreader")]
#[derive(Debug)]
pub struct AdvReader {
    pub path: PathBuf,
    pub reader: advreader::AdvReaderIter,
    pub errors: Vec<Error>,
    pub return_type: bool,
}

#[pymethods]
impl AdvReader {
    #[allow(clippy::too_many_arguments)]
    #[new]
    #[pyo3(
        signature = (path, buffer_size=None, trim=None, line_end=None, skip_comments=None, encode_comments=None, encode_strings=None, encoding=None, encoding_errors=None, extended_word_separation=None, double_double_quote_escape=None, convert2numbers=None, keep_base=None, bool_false=None, bool_true=None, return_type=None)
    )]
    fn __new__(
        path: PathBuf,
        buffer_size: Option<usize>,
        trim: Option<bool>,
        line_end: Option<u8>,
        skip_comments: Option<bool>,
        encode_comments: Option<bool>,
        encode_strings: Option<bool>,
        encoding: Option<String>,
        encoding_errors: Option<String>,
        extended_word_separation: Option<bool>,
        double_double_quote_escape: Option<bool>,
        convert2numbers: Option<bool>,
        keep_base: Option<bool>,
        bool_false: Option<Vec<u8>>,
        bool_true: Option<Vec<u8>>,
        return_type: Option<bool>,
    ) -> PyResult<Self> {
        let reader = advreader::AdvReader::new(
            &path,
            buffer_size,
            trim,
            line_end,
            skip_comments,
            encode_comments,
            encode_strings,
            encoding,
            encoding_errors,
            extended_word_separation,
            double_double_quote_escape,
            convert2numbers,
            keep_base,
            bool_false,
            bool_true,
            None,
        );
        if let Err(e) = reader {
            return Err(PyFileNotFoundError::new_err(e.to_string()));
        }
        Ok(AdvReader {
            path,
            reader: reader.unwrap().into_iter(),
            errors: Vec::new(),
            return_type: return_type.unwrap_or(false),
        })
    }

    fn __enter__(slf: PyRef<Self>) -> PyResult<PyRef<Self>> {
        Ok(slf)
    }

    #[pyo3(
        signature = (ty=None, _value=None, _traceback=None)
    )]
    fn __exit__(
        &mut self,
        ty: Option<&Bound<PyType>>,
        _value: Option<&Bound<PyAny>>,
        _traceback: Option<&Bound<PyAny>>,
        py: Python,
    ) -> PyResult<bool> {
        if let Err(e) = self.reader.stop() {
            return Err(PyValueError::new_err(e.to_string()));
        }
        match ty {
            Some(ty) => {
                if ty.eq(py.get_type::<PyValueError>()).unwrap() {
                    Ok(true)
                } else {
                    Ok(false)
                }
            }
            None => Ok(false),
        }
    }

    fn stop(&mut self) -> PyResult<bool> {
        if let Err(e) = self.reader.stop() {
            return Err(PyValueError::new_err(e.to_string()));
        }
        Ok(true)
    }

    fn next(&mut self, py: Python) -> PyResult<PyObject> {
        match self.reader.next() {
            Some(s) => match s {
                Ok(v) => {
                    if self.return_type {
                        return Ok(PyTuple::new(
                            py,
                            &match v {
                                AdvReturnValue::Bytes(w) => [
                                    AdvReturnValueEnum::Bytes
                                        .into_pyobject(py)
                                        .unwrap()
                                        .into_any()
                                        .unbind(),
                                    w.into_pyobject(py).unwrap().into_any().unbind(),
                                ],
                                AdvReturnValue::String(w) => [
                                    AdvReturnValueEnum::String
                                        .into_pyobject(py)
                                        .unwrap()
                                        .into_any()
                                        .unbind(),
                                    w.into_pyobject(py).unwrap().into_any().unbind(),
                                ],
                                AdvReturnValue::Comment(w) => [
                                    AdvReturnValueEnum::Comment
                                        .into_pyobject(py)
                                        .unwrap()
                                        .into_any()
                                        .unbind(),
                                    w.into_pyobject(py).unwrap().into_any().unbind(),
                                ],

                                AdvReturnValue::LineComment(w) => [
                                    AdvReturnValueEnum::LineComment
                                        .into_pyobject(py)
                                        .unwrap()
                                        .into_any()
                                        .unbind(),
                                    w.into_pyobject(py).unwrap().into_any().unbind(),
                                ],

                                AdvReturnValue::StringUtf8(w) => [
                                    AdvReturnValueEnum::StringUtf8
                                        .into_pyobject(py)
                                        .unwrap()
                                        .into_any()
                                        .unbind(),
                                    w.into_pyobject(py).unwrap().into_any().unbind(),
                                ],

                                AdvReturnValue::CommentUtf8(w) => [
                                    AdvReturnValueEnum::CommentUtf8
                                        .into_pyobject(py)
                                        .unwrap()
                                        .into_any()
                                        .unbind(),
                                    w.into_pyobject(py).unwrap().into_any().unbind(),
                                ],

                                AdvReturnValue::LineCommentUtf8(w) => [
                                    AdvReturnValueEnum::LineCommentUtf8
                                        .into_pyobject(py)
                                        .unwrap()
                                        .into_any()
                                        .unbind(),
                                    w.into_pyobject(py).unwrap().into_any().unbind(),
                                ],

                                AdvReturnValue::Bool(w) => [
                                    AdvReturnValueEnum::Bool
                                        .into_pyobject(py)
                                        .unwrap()
                                        .into_any()
                                        .unbind(),
                                    w.into_pyobject(py).unwrap().as_any().clone().unbind(),
                                ],

                                AdvReturnValue::Int(w) => [
                                    AdvReturnValueEnum::Int
                                        .into_pyobject(py)
                                        .unwrap()
                                        .into_any()
                                        .unbind(),
                                    w.into_pyobject(py).unwrap().into_any().unbind(),
                                ],

                                AdvReturnValue::Float(w) => [
                                    AdvReturnValueEnum::Float
                                        .into_pyobject(py)
                                        .unwrap()
                                        .into_any()
                                        .unbind(),
                                    w.into_pyobject(py).unwrap().into_any().unbind(),
                                ],

                                AdvReturnValue::Hex(w) => [
                                    AdvReturnValueEnum::Hex
                                        .into_pyobject(py)
                                        .unwrap()
                                        .into_any()
                                        .unbind(),
                                    w.into_pyobject(py).unwrap().into_any().unbind(),
                                ],

                                AdvReturnValue::Oct(w) => [
                                    AdvReturnValueEnum::Oct
                                        .into_pyobject(py)
                                        .unwrap()
                                        .into_any()
                                        .unbind(),
                                    w.into_pyobject(py).unwrap().into_any().unbind(),
                                ],

                                AdvReturnValue::Bin(w) => [
                                    AdvReturnValueEnum::Bin
                                        .into_pyobject(py)
                                        .unwrap()
                                        .into_any()
                                        .unbind(),
                                    w.into_pyobject(py).unwrap().into_any().unbind(),
                                ],

                                AdvReturnValue::Block(w) => [
                                    AdvReturnValueEnum::Bin
                                        .into_pyobject(py)
                                        .unwrap()
                                        .into_any()
                                        .unbind(),
                                    w.into_pyobject(py).unwrap().into_any().unbind(),
                                ],
                            },
                        )?
                        .into_any()
                        .unbind());
                    } else {
                        Ok(match v {
                            AdvReturnValue::Bytes(w) => w.into_pyobject(py).unwrap().into_any(),
                            AdvReturnValue::String(w) => w.into_pyobject(py).unwrap().into_any(),
                            AdvReturnValue::Comment(w) => w.into_pyobject(py).unwrap().into_any(),
                            AdvReturnValue::LineComment(w) => {
                                w.into_pyobject(py).unwrap().into_any()
                            }
                            AdvReturnValue::StringUtf8(w) => {
                                w.into_pyobject(py).unwrap().into_any()
                            }
                            AdvReturnValue::CommentUtf8(w) => {
                                w.into_pyobject(py).unwrap().into_any()
                            }
                            AdvReturnValue::LineCommentUtf8(w) => {
                                w.into_pyobject(py).unwrap().into_any()
                            }
                            AdvReturnValue::Bool(w) => {
                                w.into_pyobject(py).unwrap().as_any().clone()
                            }
                            AdvReturnValue::Int(w) => w.into_pyobject(py).unwrap().into_any(),
                            AdvReturnValue::Float(w) => w.into_pyobject(py).unwrap().into_any(),
                            AdvReturnValue::Hex(w) => w.into_pyobject(py).unwrap().into_any(),
                            AdvReturnValue::Oct(w) => w.into_pyobject(py).unwrap().into_any(),
                            AdvReturnValue::Bin(w) => w.into_pyobject(py).unwrap().into_any(),
                            AdvReturnValue::Block(w) => w.into_pyobject(py).unwrap().into_any(),
                        }
                        .unbind())
                    }
                }
                Err(e) => Err(PyValueError::new_err(e.to_string())),
            },
            None => Ok(py.None()),
        }
    }

    #[getter]
    fn path(&self) -> PyResult<String> {
        Ok(self.path.clone().into_os_string().into_string().unwrap())
    }

    #[getter]
    fn line_nr(&self) -> PyResult<usize> {
        Ok(self.reader.line_nr())
    }

    #[getter]
    fn errors(&self) -> PyResult<Vec<String>> {
        let mut errors: Vec<String> = Vec::new();
        for error in self.errors.iter() {
            errors.push(format!("{}", error));
        }
        Ok(errors)
    }
}

#[pymodule]
#[pyo3(name = "pyadvreader")]
fn init(_py: Python, m: &Bound<PyModule>) -> PyResult<()> {
    m.add("__version__", env!("CARGO_PKG_VERSION"))?;
    m.add_class::<AdvReader>()?;
    m.add_class::<ReturnValueEnum>()?;
    Ok(())
}
