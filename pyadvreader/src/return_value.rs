use pyo3::prelude::*;

#[pyclass(eq, module = "pyadvreader")]
#[derive(Debug, Clone, PartialEq)]
pub enum AdvReturnValueEnum {
    Bytes,
    String,
    Comment,
    LineComment,
    StringUtf8,
    CommentUtf8,
    LineCommentUtf8,
    Bool,
    Int,
    Float,
    Hex,
    Oct,
    Bin,
}

#[pyclass(module = "pyadvreader")]
#[derive(Debug)]
pub struct ReturnValueEnum {}

#[allow(non_snake_case)]
#[pymethods]
impl ReturnValueEnum {
    #[classattr]
    fn BYTES() -> i32 {
        0
    }

    #[classattr]
    fn STRING() -> i32 {
        1
    }

    #[classattr]
    fn COMMENT() -> i32 {
        2
    }

    #[classattr]
    fn LINECOMMENT() -> i32 {
        3
    }

    #[classattr]
    fn STRINGUTF8() -> i32 {
        4
    }

    #[classattr]
    fn COMMENTUTF8() -> i32 {
        5
    }

    #[classattr]
    fn LINECOMMENTUTF8() -> i32 {
        6
    }

    #[classattr]
    fn BOOL() -> i32 {
        7
    }

    #[classattr]
    fn INT() -> i32 {
        8
    }

    #[classattr]
    fn FLOAT() -> i32 {
        9
    }

    #[classattr]
    fn HEX() -> i32 {
        10
    }

    #[classattr]
    fn OCT() -> i32 {
        11
    }

    #[classattr]
    fn BIN() -> i32 {
        12
    }

    #[staticmethod]
    fn toStr(num: i32) -> String {
        match num {
            0 => "BYTES".to_owned(),
            1 => "STRING".to_owned(),
            2 => "COMMENT".to_owned(),
            3 => "LINECOMMENT".to_owned(),
            4 => "STRINGUTF8".to_owned(),
            5 => "COMMENTUTF8".to_owned(),
            6 => "LINECOMMENTUTF8".to_owned(),
            7 => "BOOL".to_owned(),
            8 => "INT".to_owned(),
            9 => "FLOAT".to_owned(),
            10 => "HEX".to_owned(),
            11 => "OCT".to_owned(),
            12 => "BIN".to_owned(),
            _ => "?".to_owned(),
        }
    }
}
